FROM test_ubuntu

MAINTAINER Nguyen Huu Kim <kimnguyen.ict@gmail.com>

ENV DOCKER_USER_ID 1000
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

RUN apt-get update && \
    apt-get install -y apache2 curl libapache2-mod-php

RUN a2enmod rewrite proxy proxy_fcgi && \
    service apache2 restart

COPY virtualhost.conf /etc/apache2/sites-enabled/virtualhost.conf

CMD usermod -u $DOCKER_USER_ID www-data && \
    /usr/sbin/apache2ctl -D FOREGROUND

EXPOSE 80
